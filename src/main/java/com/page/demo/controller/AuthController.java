package com.page.demo.controller;

import com.page.demo.constant.ApiRoute;
import com.page.demo.dto.LoginDto;
import com.page.demo.dto.UserDetailsDto;
import com.page.demo.service.UseraccountService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by David on 09 Jul, 2021
 **/
@RestController
@CrossOrigin
@AllArgsConstructor
public class AuthController {
    private UseraccountService useraccountService;

    @PostMapping(value = ApiRoute.LOGIN)
    public ResponseEntity<?> login(@RequestBody @Valid LoginDto loginDto) {
        UserDetailsDto dto = useraccountService.login(loginDto);
        return ResponseEntity.accepted().body(dto);
    }
}
