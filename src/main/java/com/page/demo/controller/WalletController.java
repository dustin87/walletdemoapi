package com.page.demo.controller;

import com.page.demo.constant.ApiRoute;
import com.page.demo.dao.UseraccountDao;
import com.page.demo.dao.WalletTransactionDao;
import com.page.demo.dto.FundWalletOfSecondPartyReq;
import com.page.demo.dto.FundWalletReq;
import com.page.demo.dto.GeneralResponse;
import com.page.demo.dto.WalletDto;
import com.page.demo.entity.Useraccount;
import com.page.demo.service.UseraccountService;
import com.page.demo.service.WalletService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by David on 09 Jul, 2021
 **/
@RestController
@CrossOrigin
@AllArgsConstructor
public class WalletController {
    private UseraccountService useraccountService;
    private UseraccountDao useraccountDao;
    private WalletService walletService;
    private WalletTransactionDao walletTransactionDao;

    @GetMapping(value = ApiRoute.AUTH + ApiRoute.USER + ApiRoute.WALLET_BALANCE)
    public ResponseEntity<?> getWalletBalance(Authentication auth) {

        Useraccount user = useraccountDao.findByEmail(auth.getName());

        WalletDto walletDto = new WalletDto(user.getWallet().getId(),
                user.getWallet().getAmount());

        return ResponseEntity.ok(walletDto);
    }

    @PostMapping(value = ApiRoute.AUTH + ApiRoute.USER + ApiRoute.FUND_WALLET)
    public ResponseEntity<?> fundWallet(Authentication auth,
                                        @RequestBody @Valid FundWalletReq req) {

        walletService.creditWallet(
                useraccountDao.findByEmail(auth.getName()),
                req.getAmount(), "Wallet funding");

        return ResponseEntity.ok(new GeneralResponse(
                HttpStatus.OK.value(),
                "Wallet credited successfully"));
    }

    @PostMapping(value = ApiRoute.AUTH + ApiRoute.USER + ApiRoute.WALLET + ApiRoute.CREDIT_SECOND_PARTY)
    public ResponseEntity<?> fundWalletOfSecondParty(Authentication auth,
                                                     @RequestBody @Valid FundWalletOfSecondPartyReq req) {

        walletService.fundSecondPartyWallet(req,
                useraccountDao.findByEmail(auth.getName()));

        return ResponseEntity.ok(new GeneralResponse(
                HttpStatus.OK.value(),
                "Funds transfer successful"));
    }

    @GetMapping(value = ApiRoute.AUTH + ApiRoute.USER + ApiRoute.WALLET_TRANSACTIONS)
    public ResponseEntity<?> getWalletTransactions(Authentication auth) {

        Useraccount user = useraccountDao.findByEmail(auth.getName());


        return ResponseEntity.ok(walletTransactionDao.getAllByWalletOrderByCreatedAtDesc(user.getWallet()));
    }

}
