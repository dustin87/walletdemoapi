package com.page.demo.controller;

import com.page.demo.constant.ApiRoute;
import com.page.demo.dao.UseraccountDao;
import com.page.demo.dto.*;
import com.page.demo.entity.Useraccount;
import com.page.demo.mapper.UserMapper;
import com.page.demo.service.UseraccountService;
import com.page.demo.service.WalletService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by David on 09 Jul, 2021
 **/
@RestController
@CrossOrigin
@AllArgsConstructor
public class UserController {

    private UseraccountService useraccountService;
    private UseraccountDao useraccountDao;
    private WalletService walletService;
    private ModelMapper modelMapper;

    @PostMapping(value = ApiRoute.REGISTER)
    public ResponseEntity<?> registerUser(@RequestBody @Valid RegisterUserReq req) {

        useraccountService.registerUser(req);

        return new ResponseEntity<>(new GeneralResponse(HttpStatus.CREATED.value(),
                "User registered"), HttpStatus.CREATED);
    }

    @GetMapping(value = ApiRoute.AUTH + ApiRoute.USERS)
    public ResponseEntity<?> getAllUsers(Authentication auth) {

        // get authenticated user
        Useraccount authenticatedUser = useraccountDao.findByEmail(auth.getName());

        // get all users
        List<Useraccount> users = useraccountDao.findAll()
                .stream()
                .filter(s -> !s.getId().equals(authenticatedUser.getId()))
                .collect(Collectors.toList());

        return ResponseEntity.ok(UserMapper.mapToDtoList(users, modelMapper));
    }

}
