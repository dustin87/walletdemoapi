package com.page.demo.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@Entity
public class WalletTransaction {
    public enum TransactionType{CREDIT, DEBIT}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    private Double amount;

    private String transactionDescription;


    @CreationTimestamp
    private LocalDateTime createdAt;

    @ManyToOne
    @JoinColumn(name = "wallet", referencedColumnName = "id")
    private Wallet wallet;
}
