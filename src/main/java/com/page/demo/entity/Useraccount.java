package com.page.demo.entity;

import com.page.demo.constant.EntityStatus;
import com.page.demo.constant.RoleConstant;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by David on 09 Jul, 2021
 **/
@Data
@Entity
public class Useraccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fullName;

    private String email;

    private String password;

    @Enumerated(EnumType.STRING)
    private RoleConstant role;

    @Enumerated(EnumType.STRING)
    private EntityStatus status;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @OneToOne
    private Wallet wallet;
}
