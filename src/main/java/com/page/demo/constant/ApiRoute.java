package com.page.demo.constant;

public interface ApiRoute {

	String AUTH = "/auth";

	String ADMIN = "/admin";

	String LOGIN = "/login";

	String REGISTER = "/register";

	String ACTUATOR = "/actuator";

	String USER = "/user";

	String USERS = "/users";

	String WALLET = "/wallet";

	String WALLET_BALANCE = "/wallet-balance";

	String FUND_WALLET = "/fund-wallet";

	String CREDIT_SECOND_PARTY = "/credit-second-party";

	String WALLET_TRANSACTIONS = "/wallet-transactions";

}
