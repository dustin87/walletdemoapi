package com.page.demo.constant;

public enum EntityStatus {

	ACTIVE, INACTIVE, DELETED

}
