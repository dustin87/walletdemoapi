package com.page.demo.dao;

import com.page.demo.entity.Useraccount;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by David on 09 Jul, 2021
 **/
public interface UseraccountDao extends JpaRepository<Useraccount, Long> {
    Useraccount findByEmail(String email);

    Boolean existsByEmail(String email);
}
