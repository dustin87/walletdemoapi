package com.page.demo.dao;

import com.page.demo.entity.Useraccount;
import com.page.demo.entity.Wallet;
import com.page.demo.entity.WalletTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by David on 09 Jul, 2021
 **/
public interface WalletTransactionDao extends JpaRepository<WalletTransaction, Long> {

    List<WalletTransaction> getAllByWalletOrderByCreatedAtDesc(Wallet wallet);
}
