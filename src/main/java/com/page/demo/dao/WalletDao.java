package com.page.demo.dao;

import com.page.demo.entity.Useraccount;
import com.page.demo.entity.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by David on 09 Jul, 2021
 **/
public interface WalletDao extends JpaRepository<Wallet, Long> {
}
