package com.page.demo.mapper;

import com.page.demo.dto.UserDto;
import com.page.demo.entity.Useraccount;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by David on 09 Jul, 2021
 **/
public class UserMapper {
    public static UserDto mapToDto(Useraccount entity, ModelMapper mapper) {
        if (entity == null)
            return null;

        return mapper.map(entity, UserDto.class);
    }

    public static List<UserDto> mapToDtoList(List<Useraccount> entityList, ModelMapper mapper) {
        return entityList.stream()
                .map(s -> mapToDto(s, mapper))
                .collect(Collectors.toList());
    }
}
