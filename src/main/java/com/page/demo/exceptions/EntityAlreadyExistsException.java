package com.page.demo.exceptions;

public class EntityAlreadyExistsException extends RuntimeException {

	public EntityAlreadyExistsException(String message) {
		super(message);
	}

	public EntityAlreadyExistsException() {
		super("entity already exists");
	}

}
