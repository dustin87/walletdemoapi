package com.page.demo.service;

import com.page.demo.entity.Wallet;
import com.page.demo.entity.WalletTransaction;

/**
 * Created by David on 09 Jul, 2021
 **/
public interface WalletTransactionService {
    WalletTransaction create(WalletTransaction.TransactionType transactionType,
                             Wallet wallet,
                             Double amount,
                             String description);
}
