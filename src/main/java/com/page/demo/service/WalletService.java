package com.page.demo.service;

import com.page.demo.dto.FundWalletOfSecondPartyReq;
import com.page.demo.entity.Useraccount;
import com.page.demo.entity.Wallet;

/**
 * Created by David on 09 Jul, 2021
 **/
public interface WalletService {
    Wallet create();

    void creditWallet(Useraccount acct, Double amountTobeCredited,  String transactionDescription);

    void debitWallet(Useraccount acct, Double amountToBeDebited,
                     String transactionDescription);

    void fundSecondPartyWallet(FundWalletOfSecondPartyReq req,
                               Useraccount fundedBy);
}
