package com.page.demo.service;

import com.page.demo.dto.LoginDto;
import com.page.demo.dto.RegisterUserReq;
import com.page.demo.dto.UserDetailsDto;

/**
 * Created by David on 09 Jul, 2021
 **/
public interface UseraccountService {
    UserDetailsDto login(LoginDto dto);

    void registerUser(RegisterUserReq req);
}
