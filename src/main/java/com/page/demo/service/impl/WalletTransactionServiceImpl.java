package com.page.demo.service.impl;

import com.page.demo.dao.WalletTransactionDao;
import com.page.demo.entity.Wallet;
import com.page.demo.entity.WalletTransaction;
import com.page.demo.service.WalletTransactionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by David on 09 Jul, 2021
 **/
@Service
@AllArgsConstructor
@Transactional
public class WalletTransactionServiceImpl implements WalletTransactionService {

    private WalletTransactionDao walletTransactionDao;

    @Override
    public WalletTransaction create(WalletTransaction.TransactionType transactionType,
                                    Wallet wallet,
                                    Double amount, String description) {

        WalletTransaction transaction = new WalletTransaction();
        transaction.setAmount(amount);
        transaction.setTransactionDescription(description);
        transaction.setTransactionType(transactionType);
        transaction.setWallet(wallet);

        return walletTransactionDao.save(transaction);
    }
}
