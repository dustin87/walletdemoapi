package com.page.demo.service.impl;

import com.page.demo.dao.UseraccountDao;
import com.page.demo.dao.WalletDao;
import com.page.demo.dto.FundWalletOfSecondPartyReq;
import com.page.demo.entity.Useraccount;
import com.page.demo.entity.Wallet;
import com.page.demo.entity.WalletTransaction;
import com.page.demo.exceptions.ConflictException;
import com.page.demo.exceptions.NotFoundException;
import com.page.demo.service.WalletService;
import com.page.demo.service.WalletTransactionService;
import lombok.AllArgsConstructor;
import lombok.Synchronized;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by David on 09 Jul, 2021
 **/
@Service
@AllArgsConstructor
@Transactional
public class WalletServiceImpl implements WalletService {

    private WalletDao walletDao;
    private WalletTransactionService walletTransactionService;
    private UseraccountDao useraccountDao;

    @Override
    public Wallet create() {
        Wallet w = new Wallet();
        w.setAmount(0.0);
        return walletDao.save(w);
    }

    @Override
    public void creditWallet(Useraccount acct, Double amountTobeCredited, String transactionDescription) {
        Wallet wallet = acct.getWallet();
        wallet.setAmount(wallet.getAmount() + amountTobeCredited);
        walletDao.save(wallet);

        // create wallet transaction
        walletTransactionService.create(WalletTransaction.TransactionType.CREDIT,
                wallet, amountTobeCredited, transactionDescription);
    }

    @Synchronized
    @Override
    public void debitWallet(Useraccount acct, Double amountToBeDebited,
                            String transactionDescription) {
        // validate
        Wallet wallet = acct.getWallet();
        if (wallet.getAmount() < amountToBeDebited) {
            throw new ConflictException("Insufficient funds");
        }

        wallet.setAmount(wallet.getAmount() - amountToBeDebited);
        walletDao.save(wallet);

        // create wallet transaction
        walletTransactionService.create(WalletTransaction.TransactionType.DEBIT,
                wallet, amountToBeDebited, transactionDescription);
    }

    @Override
    public void fundSecondPartyWallet(FundWalletOfSecondPartyReq req, Useraccount fundedBy) {
        // debit owners wallet
        debitWallet(fundedBy, req.getAmount(), "Second party wallet funding");

        // credit new Users wallet
        Useraccount secondParty = useraccountDao.findById(req.getUseraccountId())
                .orElseThrow(() -> new NotFoundException("User not found"));

        creditWallet(secondParty, req.getAmount(), "Second party wallet funding");
    }
}
