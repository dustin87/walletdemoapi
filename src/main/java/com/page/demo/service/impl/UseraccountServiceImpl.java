package com.page.demo.service.impl;

import com.page.demo.constant.EntityStatus;
import com.page.demo.constant.RoleConstant;
import com.page.demo.dao.UseraccountDao;
import com.page.demo.dto.LoginDto;
import com.page.demo.dto.RegisterUserReq;
import com.page.demo.dto.UserDetailsDto;
import com.page.demo.entity.Useraccount;
import com.page.demo.exceptions.EmailAlreadyExistsException;
import com.page.demo.security.TokenProvider;
import com.page.demo.service.UseraccountService;
import com.page.demo.service.WalletService;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by David on 09 Jul, 2021
 **/
@Service
@AllArgsConstructor
@Transactional
public class UseraccountServiceImpl implements UseraccountService {

    private UseraccountDao useraccountDao;
    private AuthenticationManager authenticationManager;
    private TokenProvider tokenProvider;
    private PasswordEncoder passwordEncoder;
    private WalletService walletService;

    @Override
    public UserDetailsDto login(LoginDto dto) {
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(dto.getEmail(),
                        dto.getPassword()));

        Useraccount acct = useraccountDao.findByEmail(authentication.getName());


        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = tokenProvider.generateToken(authentication);

        // prepare response
        UserDetailsDto userDetails = new UserDetailsDto();
        userDetails.setToken(token);
        userDetails.setEmail(acct.getEmail());
        userDetails.setRole(acct.getRole());
        userDetails.setFullName(acct.getFullName());

        return userDetails;
    }

    @Override
    public void registerUser(RegisterUserReq req) {
        if (useraccountDao.existsByEmail(req.getEmail())) {
            throw new EmailAlreadyExistsException();
        }

        Useraccount user = new Useraccount();
        user.setEmail(req.getEmail().trim());
        user.setPassword(passwordEncoder.encode(req.getPassword().trim()));
        user.setFullName(req.getFullName().trim());
        user.setRole(RoleConstant.PORTAL_USER);
        user.setStatus(EntityStatus.ACTIVE);
        user.setWallet(walletService.create());
        useraccountDao.save(user);

    }
}
