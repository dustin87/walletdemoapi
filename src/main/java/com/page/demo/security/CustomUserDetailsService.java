package com.page.demo.security;

import com.page.demo.constant.EntityStatus;
import com.page.demo.dao.UseraccountDao;
import com.page.demo.entity.Useraccount;
import com.page.demo.exceptions.ForbiddenException;
import com.page.demo.exceptions.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;


@Service
@Qualifier("customUserDetailsService")
@AllArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

	private UseraccountDao useraccountDao;



	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Useraccount account = useraccountDao.findByEmail(email);

		// validate account
		if (account == null)
			throw new NotFoundException("Invalid credentials");


		if (!account.getStatus().equals(EntityStatus.ACTIVE)) {
			throw new ForbiddenException("Account is NOT active");
		}

		else {
			String role = account.getRole().name();

			Set<GrantedAuthority> grantedAuthorities = new HashSet();
			grantedAuthorities.add(new SimpleGrantedAuthority(role));

			return new User(account.getEmail(), account.getPassword(),
					grantedAuthorities);
		}
	}

}
