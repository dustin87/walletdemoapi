package com.page.demo.util;

import org.apache.logging.log4j.util.Strings;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by David on 04 Sep, 2020
 **/
public class GeneralUtil {

	private static final long serialVersionUID = 8034180083191221753L;

	private static final char[] alphaNum = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
			'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
			'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

	private static final Random ran = new Random();

	private static final char[] numArr = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9' };

	public static String generateRandomString(final int length) {
		Random r = new Random();
		char[] character = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
				'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1',
				'2', '3', '4', '5', '6', '7', '8', '9', '0' };
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			char c = character[r.nextInt(35)];
			sb.append(c);
		}
		return sb.toString();
	}


	public static String generalRandomNumbers(final int length) {
		Random r = new Random(); // perhaps make it a class variable so you don't make a
		// new one every time
		char[] character = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			char c = character[r.nextInt(9)];
			sb.append(c);
		}
		return sb.toString();
	}

	public static String generatePropertyRef() {
		return "box" + generateRandomString(10).toLowerCase();
	}

	public static String generateBidREf() {
		return "bid" + generateRandomString(10).toLowerCase();
	}

	public static String generateTransactionRef() {
		return "trans" + generateRandomString(10).toLowerCase();
	}

	public static String getEmailVerificationCode() {
		return generateRandomString(5).toUpperCase();
	}

	public static Double convertToTwoDecimalPlaces(double number)
	{
		DecimalFormat df =  new DecimalFormat("#.00");
		String formatedNo = df.format(number);
		return Double.valueOf(formatedNo);
	}

	public static Boolean checkIfStringContainsSpecialCharacters(String mystring) {
		if (Strings.isEmpty(mystring)) {
			return false;
		}

		Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(mystring);

		return m.find();
	}


}

