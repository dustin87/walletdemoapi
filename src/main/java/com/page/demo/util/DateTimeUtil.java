package com.page.demo.util;

import com.page.demo.constant.DateTimeEnum;
import org.apache.logging.log4j.util.Strings;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {

	public static LocalDateTime getDateTime() {
		return LocalDateTime.now(getTimeZone());
	}

	public static LocalDate getDate() {
		return LocalDate.now(getTimeZone());
	}

	public static String getDateTime(LocalDateTime dt) {
		if (dt == null)
			return null;
		return dt.format(getDateTimeFormatter());
	}

	public static String getDate(LocalDate d) {
		return d.format(getDateFormatter());
	}

	public static LocalDateTime getDateTime(String dt) {
		return LocalDateTime.parse(dt,
				DateTimeFormatter.ofPattern(DateTimeEnum.DATE_TIME_PATTERN));
	}

	public static LocalDate getDate(String d) {
		if (Strings.isEmpty(d))
			return null;
		return LocalDate.parse(d, DateTimeFormatter.ofPattern(DateTimeEnum.DATE_PATTERN));
	}

	public static DateTimeFormatter getDateTimeFormatter() {
		return DateTimeFormatter.ofPattern(DateTimeEnum.DATE_TIME_PATTERN);
	}

	public static DateTimeFormatter getDateFormatter() {
		return DateTimeFormatter.ofPattern(DateTimeEnum.DATE_PATTERN);
	}

	public static ZoneOffset getTimeZone() {
		return ZoneOffset.of("+1");
	}

}
