package com.page.demo.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * Created by David on 09 Jul, 2021
 **/
@Data
public class RegisterUserReq {

    @NotEmpty(message = "Full name is required")
    private String fullName;

    @NotEmpty(message = "Email is required")
    @Email(message = "Kindly use a valid email")
    private String email;

    @NotEmpty(message = "Password is required")
    private String password;
}
