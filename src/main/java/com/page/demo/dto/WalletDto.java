package com.page.demo.dto;

import lombok.Data;


@Data
public class WalletDto {
    private long id;
    private Double amount;

    public WalletDto(long id, Double amount){
        this.id = id;
        this.amount = amount;
    }
}
