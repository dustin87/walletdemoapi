package com.page.demo.dto;

import com.page.demo.constant.EntityStatus;
import com.page.demo.constant.RoleConstant;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

/**
 * Created by David on 09 Jul, 2021
 **/
@Data
public class UserDto {
    private Long id;

    private String fullName;

    private String email;

    private RoleConstant role;

    private EntityStatus status;

    private LocalDateTime createdAt;
}
