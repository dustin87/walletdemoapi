package com.page.demo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by David on 09 Jul, 2021
 **/
@Data
public class FundWalletReq {

    @NotNull(message = "Amount is required")
    private Double amount;
}
