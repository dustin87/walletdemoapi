package com.page.demo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by David on 09 Jul, 2021
 **/
@Data
public class FundWalletOfSecondPartyReq {

    @NotNull(message = "Useraccount ID for second party is required")
    private Long useraccountId;

    @NotNull(message = "Amount is required")
    private Double amount;
}
