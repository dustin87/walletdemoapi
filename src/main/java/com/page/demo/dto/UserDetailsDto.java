package com.page.demo.dto;

import com.page.demo.constant.RoleConstant;
import lombok.Data;

/**
 * Created By David on 26-May-2020
 **/

@Data
public class UserDetailsDto {

	private String email;

	private String token;

	private String fullName;

	private RoleConstant role;

}
