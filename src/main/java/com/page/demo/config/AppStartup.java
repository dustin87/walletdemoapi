package com.page.demo.config;

import com.google.gson.Gson;
import com.page.demo.dao.UseraccountDao;
import com.page.demo.dao.WalletDao;
import com.page.demo.dao.WalletTransactionDao;
import com.page.demo.dto.RegisterUserReq;
import com.page.demo.service.UseraccountService;
import com.page.demo.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


@Component
@Transactional
public class AppStartup implements ApplicationListener<ApplicationReadyEvent> {


	@Autowired
	private UseraccountService useraccountService;

	@Autowired
	private UseraccountDao useraccountDao;

	@Autowired
	private WalletService walletService;

	@Autowired
    @Lazy
	private PasswordEncoder passwordEncoder;

	@Autowired
	private WalletDao walletDao;


	@Override
	public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
		createTestUsers();
	}

	private void createTestUsers() {
		if (useraccountDao.findByEmail("tom1@gmail.com") == null) {
			RegisterUserReq req = new RegisterUserReq();
			req.setEmail("tom1@gmail.com");
			req.setFullName("John Morrison");
			req.setPassword("password");

			useraccountService.registerUser(req);

			req = new RegisterUserReq();
			req.setEmail("tom2@gmail.com");
			req.setFullName("Philip Dean");
			req.setPassword("password");

			useraccountService.registerUser(req);
		}
	}

	private void testData() {



		/*Useraccount acct = useraccountDao.findByEmail("optisoftuser@gmail.com");
		BankAccount ba = bankAccountDao.findByUseraccount(acct);
		try {
			paymentService.createTransferRecipient(null);
		} catch (IOException e) {
			e.printStackTrace();
		}*/

	}





}
