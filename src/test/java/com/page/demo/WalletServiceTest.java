package com.page.demo;

import com.page.demo.constant.EntityStatus;
import com.page.demo.dao.UseraccountDao;
import com.page.demo.dao.WalletDao;
import com.page.demo.dto.RegisterUserReq;
import com.page.demo.entity.Useraccount;
import com.page.demo.entity.Wallet;
import com.page.demo.entity.WalletTransaction;
import com.page.demo.exceptions.ConflictException;
import com.page.demo.exceptions.EmailAlreadyExistsException;
import com.page.demo.service.WalletTransactionService;
import com.page.demo.service.impl.UseraccountServiceImpl;
import com.page.demo.service.impl.WalletServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Created by David on 10 Jul, 2021
 **/
@RunWith(MockitoJUnitRunner.Silent.class)
public class WalletServiceTest {

    @InjectMocks private WalletServiceImpl walletService;
    @Mock private WalletDao walletDao;
    @Mock private WalletTransactionService walletTransactionService;
    @Mock private UseraccountDao useraccountDao;

    // test data
    Useraccount acct = new Useraccount();

    @Before
    public void init() {

        Wallet wallet = new Wallet();
        wallet.setId(1L);
        wallet.setAmount(20.0);

        acct.setId(1L);
        acct.setStatus(EntityStatus.ACTIVE);
        acct.setFullName("Tom Brody");
        acct.setWallet(wallet);
    }


    @Test
    public void createWalletSuccessfully() {
        walletService.create();
    }

    @Test
    public void creditWalletSuccessfully() {
        // expectations
        Mockito.when(walletTransactionService.create(WalletTransaction.TransactionType.CREDIT,
                acct.getWallet(), 20.0, "Description")).thenReturn(new WalletTransaction());

        walletService.creditWallet(acct, 20.0, "Description");
    }


    @Test(expected = ConflictException.class)
    public void debitWalletThrowException() {
        walletService.debitWallet(acct, 200.0, "Description");
    }

    @Test
    public void debitWalletSuccessfully() {
        walletService.debitWallet(acct, 19.0, "Description");
    }
}


