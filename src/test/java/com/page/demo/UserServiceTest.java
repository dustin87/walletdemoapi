package com.page.demo;

import com.page.demo.dao.UseraccountDao;
import com.page.demo.dto.RegisterUserReq;
import com.page.demo.exceptions.EmailAlreadyExistsException;
import com.page.demo.security.TokenProvider;
import com.page.demo.service.WalletService;
import com.page.demo.service.impl.UseraccountServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collection;

/**
 * Created by David on 10 Jul, 2021
 **/
@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceTest {

    @InjectMocks private UseraccountServiceImpl useraccountService;

    @Mock private UseraccountDao useraccountDao;
    @Mock private AuthenticationManager authenticationManager;
    @Mock private TokenProvider tokenProvider;
    @Mock private PasswordEncoder passwordEncoder;
    @Mock private WalletService walletService;


    // test data
    RegisterUserReq req = new RegisterUserReq();

    @Before
    public void init() {
        req.setEmail("jerry@gmail.com");
        req.setPassword("password");
        req.setFullName("Jerry Green");
    }


    @Test(expected = EmailAlreadyExistsException.class)
    public void registerUserThrowsException() {

        // expectations
        Mockito.when(useraccountDao.existsByEmail(req.getEmail())).thenReturn(true);

        useraccountService.registerUser(req);

    }

    @Test
    public void registerUserSuccessfully() {

        // expectations
        Mockito.when(useraccountDao.existsByEmail(req.getEmail())).thenReturn(false);

        useraccountService.registerUser(req);
    }
}
